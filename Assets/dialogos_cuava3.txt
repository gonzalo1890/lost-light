Perdido 1 Nos apagamos con el tiempo, dejamos de intentarlo y creamos esta cueva oscura y solitaria donde nadie puede herirnos.
Aún asi dentro de todos nosotros queda un poco de ella, enjaulada en una caja negra, una caja de espinas. Sellada 
con miedo y dolor.

Capa 1: Mi luz ya no es suficiente, siento que me apago.
Se llevan mi luz, detienen la oscuridad. No durará demasiado. Hay que correr

Capa 2: Tengo miedo

Monje: El mundo de los sueños y las esperanzas solo nos rompe. 
Quiebra nuestro espiritu. 
Aqui somos capaces de ser nosotros. 
Y tu... dispondrias tu sacrificio por una esperanza incierta?
Y si eso te lleva a perder lo que te puso de pie inicialmente?
...

Monje 2: Su brillo se apagará al salir....

CAPA 3: Cruza la puerta...


Dialogo Final: Al final, ninguno estuvo perdido nunca, solo esperamos por el otro.


Fin


