﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour, Imechanic
{
    public bool colision = false;
    public bool UnaVez = false;
    private bool Show = true;
    //Dialogo inicial
    public DialogueUnit dialogue_unit;
    
    //Canvas
    public GameObject dialogueUI;

    //Menu de mensaje
    [NonSerialized]
    public GameObject message_menu;
    [NonSerialized]
    public Text message_transmitter;
    [NonSerialized]
    public Text message_message;


    private bool DialogueStart = false;
    private int actual_message = 0;

    private void Start()
    {
        message_menu = dialogueUI.transform.GetChild(0).gameObject;
        message_transmitter = dialogueUI.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>();
        message_message = dialogueUI.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();
    }

    private void Update()
    {
        if(dialogue_unit != null && DialogueStart)
        {
            dialogue_unit.dialog_modules[actual_message].Tick(Time.deltaTime);
        }
    }
    public void OnHighlight()
    {
        
    }
    public void OnDrop()
    {//NO REQUERIDO        
    }



    public void StartDialogueUnit(DialogueUnit dialogueUnit)
    {
        dialogue_unit = dialogueUnit;
        actual_message = 0;
        dialogueUI.SetActive(true);
        dialogueUnit.dialog_modules[0].Play(this);
        DialogueStart = true;
    }
    public void StartNextDialogueModule()
    {
        if (dialogue_unit.dialog_modules.Length > actual_message)
        {
            dialogue_unit.dialog_modules[actual_message].Play(this);
        }
        else
        {
            StopDialogue();
        }
    }
    public void StopDialogue()
    {
        //dialogueUI.SetActive(false);
        message_menu.SetActive(false);
        actual_message = 0;
        DialogueStart = false;
    }

    public void AddActualMessage()
    {
        actual_message++;
    }

    //Efecto de texto animado, aparece caracter por caracter
    public void StartReadText(string sentence, Text text)
    {
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence, text));
    }
    IEnumerator TypeSentence(string sentence, Text text)
    {
        text.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            text.text += letter;
            yield return new WaitForSeconds(.05f);
        }
    }

    public void OnAction(Player_State ps)
    {
        if (Show)
        {
            StartDialogueUnit(dialogue_unit);
            if (UnaVez)
            {
                Show = false;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(colision)
        {
            if (Show)
            {
                StartDialogueUnit(dialogue_unit);
                if (UnaVez)
                {
                    Show = false;
                }
            }
        }
    }
}
