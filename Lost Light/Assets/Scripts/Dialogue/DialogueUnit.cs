﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "InGame/Dialog/DialogueUnit")]
public class DialogueUnit : ScriptableObject
{
    public DialogueModule[] dialog_modules;
}
