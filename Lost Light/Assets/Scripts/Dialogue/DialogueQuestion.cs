﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "InGame/Dialog/DialogueQuestion")]
public class DialogueQuestion : DialogueModule
{
    public GameObject ButtonPrefab;
    public string transmitter;
    public string question;
    public string[] answers;
    public DialogueUnit[] answers_unit;
    private Dialogue dialogue;
    public override void Play(Dialogue _dialogue)
    {
        dialogue = _dialogue;
    }
    public override void Tick(float d)
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Stop();
            dialogue.StopDialogue();
        }
    }
    public override void Stop()
    {

    }

    void ButtonAction(int numButton)
    {
        Stop();
        dialogue.StartDialogueUnit(answers_unit[numButton]);
    }
}
