﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "InGame/Dialog/DialogueEvent")]
public class DialogueEvent : DialogueModule
{
    public string transmitter;
    public string message;
    private Dialogue dialogue;
    private GameManager gm;
    public override void Play(Dialogue _dialogue)
    {
        dialogue = _dialogue;
        LoadMessage();
    }

    public override void Tick(float d)
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Stop();
            dialogue.AddActualMessage();
            dialogue.StartNextDialogueModule();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Stop();
            dialogue.StopDialogue();
        }
    }

    public override void Stop()
    {
        dialogue.message_menu.SetActive(false);
    }

    void LoadMessage()
    {
        dialogue.message_menu.SetActive(true);
        dialogue.message_transmitter.text = transmitter;
        dialogue.StartReadText(message, dialogue.message_message);
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        gm.FinalBoss();
    }

}

public enum eventType { give, receive };