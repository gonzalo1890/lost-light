﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DialogueLogic : ScriptableObject
{
    public abstract void Start();
    public abstract void OnAction();
    public abstract void OnHighLight();
}
