﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "InGame/Dialog/DialogueModule")]
public abstract class DialogueModule : ScriptableObject
{
    public abstract void Play(Dialogue _dialogue);
    public abstract void Tick(float d);
    public abstract void Stop();
}
