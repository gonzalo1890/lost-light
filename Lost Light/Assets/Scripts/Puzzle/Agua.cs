﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agua : Puerta, Imechanic
{
    public Antorcha[] Antorchas;
    public PlacaPresion[] Placas;
    private Animator anim;
    public bool isCompleted = false;
    public ParticleSystem waterMagicPart;
    public GameObject rock;
    void Start()
    {
        anim = GetComponent<Animator>();
        waterMagicPart.Stop();
    }
    public override void UpdateLogic()
    {
        if(CompletadoAntorchas())
        {
            AguaDeciende();
        }

        if (CompletadoPlacas())
        {
            AguaAsciende();
        }
    }

    public bool CompletadoAntorchas()
    {
        bool value = true;

        for (int i = 0; i < Antorchas.Length; i++)
        {
            if(Antorchas[i].isCompleted == false)
            {
                value = false;
            }
        }
        return value;
    }

    void AguaDeciende()
    {
        anim.SetBool("Desciende", true);
    }

    public bool CompletadoPlacas()
    {
        bool value = true;

        for (int i = 0; i < Placas.Length; i++)
        {
            if (Placas[i].isCompleted == false)
            {
                value = false;
            }
        }
        return value;
    }
    public override void PressureOff()
    {
        isCompleted = false;
    }
    void AguaAsciende()
    {
        waterMagicPart.Play();
        rock.SetActive(false);
        anim.SetBool("Asciende", true);
    }
    public void OnAction(Player_State ps)
    {
        
    }
}
