﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esfera : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Player_State>() != null)
        {
            other.GetComponent<Player_State>().SphereValue(1);
            Destroy(gameObject);
        }
    }
}
