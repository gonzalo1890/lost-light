﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacaPresion : MonoBehaviour
{
    public bool isCompleted = false;
    public Puerta puerta;
    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Golem>() != null)
        {
            isCompleted = true;
            puerta.UpdateLogic();
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Golem>() != null)
        {
            isCompleted = false;
            puerta.PressureOff();
        }
    }
}
