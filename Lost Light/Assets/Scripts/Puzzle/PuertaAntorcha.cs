﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaAntorcha : Puerta, Imechanic
{
    public Antorcha[] Antorchas;
    private Animator anim;
    private Renderer Runa;
    public Material emission;
    public bool isCompleted = false;
    void Start()
    {
        anim = GetComponent<Animator>();
        Runa = transform.GetChild(0).transform.GetChild(0).GetComponent<Renderer>();
    }
    public override void UpdateLogic()
    {
        if(Completado())
        {
            Finish();
        }
    }

    public bool Completado()
    {
        bool value = true;

        for (int i = 0; i < Antorchas.Length; i++)
        {
            if(Antorchas[i].isCompleted == false)
            {
                value = false;
            }
        }
        return value;
    }

    void Finish()
    {
        Runa.material = emission;
        isCompleted = true;
    }

    public void OnAction(Player_State ps)
    {
        if (isCompleted)
        {
            anim.SetBool("Open", true);
        }
    }

    public override void PressureOff()
    {
        
    }
}
