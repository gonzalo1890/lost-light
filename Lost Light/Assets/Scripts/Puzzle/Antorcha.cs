﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antorcha : MonoBehaviour, Imechanic
{
    public bool isCompleted = false; //si esta encendida
    public bool haveEsfera = false;
    private GameObject esfera;
    private ParticleSystem part;
    public Puerta puerta;
    public AudioSource audioSource;
    //public 

    void Start()
    {
        Initialize();
    }
    public void OnAction(Player_State ps)
    {
        if (haveEsfera)
        {
            isCompleted = true;
            puerta.UpdateLogic();
            audioSource.Play();
            part.Play();
        }else
        {
            if(ps.GetSphereValue() > 0 && !isCompleted)
            {
                UseSphere(ps);
            }
        }
    }

    void Initialize()
    {
        esfera = transform.GetChild(1).gameObject;
        part = transform.GetChild(2).GetComponent<ParticleSystem>();
        part.Stop();
        if (haveEsfera)
        {
            esfera.SetActive(true);
        }
        if(isCompleted)
        {

            part.Play();
        }
    }

    void UseSphere(Player_State ps)
    {
        esfera.SetActive(true);
        ps.SphereValue(-1);
        part.Play();
        audioSource.Play();
        isCompleted = true;
        puerta.UpdateLogic();
    }
}
