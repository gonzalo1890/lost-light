﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaEspejoReja : Puerta, Imechanic
{
    public Espejo[] Espejos;
    public Animator anim;
    public bool isCompleted = false;
    void Start()
    {
        //anim = GetComponent<Animator>();
    }
    public override void UpdateLogic()
    {
        if(Completado())
        {
            Finish();
        }
    }

    public bool Completado()
    {
        bool value = true;

        for (int i = 0; i < Espejos.Length; i++)
        {
            if(Espejos[i].isCompleted == false)
            {
                value = false;
            }
        }
        return value;
    }

    void Finish()
    {
        isCompleted = true;
        anim.SetBool("Open", true);
    }
    public override void PressureOff()
    {
    }

    public void OnAction(Player_State ps)
    {
    }
}
