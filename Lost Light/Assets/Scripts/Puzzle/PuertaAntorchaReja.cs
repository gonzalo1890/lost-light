﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaAntorchaReja : Puerta, Imechanic
{
    public Antorcha[] Antorchas;
    private Animator anim;
    public bool isCompleted = false;
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    public override void UpdateLogic()
    {
        if(Completado())
        {
            Finish();
        }
    }

    public bool Completado()
    {
        bool value = true;

        for (int i = 0; i < Antorchas.Length; i++)
        {
            if(Antorchas[i].isCompleted == false)
            {
                value = false;
            }
        }
        return value;
    }

    void Finish()
    {
        isCompleted = true;
        anim.SetBool("Open", true);
    }

    public void OnAction(Player_State ps)
    {

    }

    public override void PressureOff()
    {
        
    }
}
