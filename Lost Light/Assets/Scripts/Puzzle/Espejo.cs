﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espejo : MonoBehaviour, Imechanic
{
    private Transform EspejoSoporte;
    public int MovimientoActivador;
    private int movimiento = 0;
    private Renderer espejo;
    public GameObject Luz;
    public Material emission;
    public Material normal;
    public Puerta puerta;
    public bool isCompleted = false;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        EspejoSoporte = transform.GetChild(1).transform;
        espejo = transform.GetChild(1).transform.GetChild(0).GetComponent<Renderer>();
        Luz = transform.GetChild(1).transform.GetChild(0).transform.GetChild(0).gameObject;
        CalculateMechanic();
    }

    public void OnAction(Player_State ps)
    {
        EspejoSoporte.Rotate(45, 0, 0, Space.Self);

        if(movimiento <= 6)
        {
            movimiento++;
        }else
        {
            movimiento = 0;
        }
        CalculateMechanic();
    }

    void CalculateMechanic()
    {
        if (movimiento == MovimientoActivador)
        {
            espejo.material = emission;
            Luz.SetActive(true);
            isCompleted = true;
            audioSource.Play();
            puerta.UpdateLogic();
        }
        else
        {
            espejo.material = normal;
            Luz.SetActive(false);
            isCompleted = false;
        }
    }
}
