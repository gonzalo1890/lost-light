﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Puerta : MonoBehaviour
{
    public abstract void UpdateLogic();
    public abstract void PressureOff();
}
