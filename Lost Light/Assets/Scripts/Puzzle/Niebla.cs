﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Niebla : MonoBehaviour
{
    public float speed = 10;
    public GameManager gm;
    void Update()
    {
        transform.Translate(transform.right * speed * Time.deltaTime, Space.World);
    }


    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Player_State>() != null)
        {
            gm.GameOver();
        }
    }
}
