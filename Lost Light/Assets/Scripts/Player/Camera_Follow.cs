﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Follow : MonoBehaviour {

	public float CameraMoveSpeed = 120.0f;
    private Vector3 velocity = Vector3.zero;
    public GameObject CameraFollowObj;
	Vector3 FollowPOS;
	public float clampAngle = 80.0f;
	public float inputSensitivity = 150.0f;
	public GameObject PlayerObj;
	private float mouseX;
    private float mouseY;
    private float finalInputX;
    private float finalInputZ;
	public float smoothRot;
	private float rotY = 0.0f;
	private float rotX = 0.0f;
    public float RotmouseX;


    // Use this for initialization
    void Start () {
		Vector3 rot = transform.localRotation.eulerAngles;
		rotY = rot.y;
		rotX = rot.x;
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {

		// We setup the rotation of the sticks here
		float inputX = Input.GetAxis ("RightStickHorizontal");
		float inputZ = Input.GetAxis ("RightStickVertical");
		mouseX = Input.GetAxis ("Mouse X");
		mouseY = Input.GetAxis ("Mouse Y");
		finalInputX = inputX + mouseX;
		finalInputZ = inputZ + mouseY;

		rotY += finalInputX * inputSensitivity * Time.deltaTime;
		rotX += finalInputZ * inputSensitivity * Time.deltaTime;
		rotX = Mathf.Clamp (rotX, -clampAngle, clampAngle);

		Quaternion localRotation = Quaternion.Euler (rotX, rotY, 0.0f);
        transform.rotation = Quaternion.Slerp(transform.rotation, localRotation, smoothRot * Time.deltaTime);
        //transform.rotation = localRotation;
        RotmouseX = rotY;
    }

	void LateUpdate () {
		CameraUpdater ();
	}

	void CameraUpdater() {
        // set the target object to follow
        Transform target = CameraFollowObj.transform;
		float step = CameraMoveSpeed * Time.deltaTime;
        //transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, CameraMoveSpeed * Time.deltaTime);
        transform.position = Vector3.MoveTowards (transform.position, target.position, step);
	}
}
