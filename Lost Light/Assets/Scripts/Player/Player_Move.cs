﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Move : MonoBehaviour
{
    public Vector3 movement;
    public float WalkSpeed;
    public float RunSpeed;
    public float JumpForce;
    public bool Running = false;
    private float AnimMove = 0;
    private float RotY;
    public Camera_Follow cameraFollow;
    public Animator anim;
    private Rigidbody rb;
    public Transform feet;
    private bool floorDetect = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        CalculeMove();
        PisoDetect();
        if (Input.GetKeyDown(KeyCode.Space) && floorDetect) 
        {
            Jump();
        }
        float rot = cameraFollow.RotmouseX;
        RotatePlayer(rot);
    }
    void FixedUpdate()
    {
        Move();
    }
    void CalculeMove()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0 || Mathf.Abs(Input.GetAxis("Vertical")) > 0)
        {
            movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        }
        else
        {
            movement = Vector3.zero;
        }

        movement.Normalize();
        if (Running)
        {
            movement *= RunSpeed;
        }
        else
        {
            movement *= WalkSpeed;
        }


    
        float speedPercent = movement.magnitude / 2f;
        AnimMove = Mathf.Lerp(AnimMove, speedPercent, 0.3f);
        speedPercent = speedPercent / 1.5f;
        int animNum = CalculeMoveAnim(speedPercent);

        if(animNum != 0 && floorDetect)
        {
            anim.SetBool("Run", true);
        }else
        {
            anim.SetBool("Run", false);
        }

    }
    void Jump()
    {
        anim.SetBool("Jump", true);
        rb.AddForce(transform.up * JumpForce, ForceMode.Impulse);
    }
    public void Move() //Mueve en la direccion de movimiento que dan las teclas
    {
        Vector3 newPosition = rb.position + transform.TransformDirection(movement * Time.fixedDeltaTime);
        rb.MovePosition(newPosition);
    }

    void PisoDetect()
    {
        RaycastHit hit;
        if (Physics.Raycast(feet.position, -transform.up, out hit, 0.6f))
        {
            if (hit.collider.gameObject.tag == "Wall")
            {
                floorDetect = true;
                anim.SetBool("Jump", false);
            }
            else
            {
                floorDetect = false;
            }
        }
        else
        {
            floorDetect = false;
        }
    }
    int CalculeMoveAnim(float num) //Calcula segun la fuerza si esta corriendo o caminando y da la mejor animacion
    {
        int r = 0;
        if (num > 1.5f)
        {
            r = 2;
        }
        else if (num > 0.5f)
        {
            r = 1;
        }
        else
        {
            r = 0;
        }
        return r;
    }

    public void RotatePlayer(float rotY)
    {
        RotY = rotY;
        transform.rotation = Quaternion.Euler(0, RotY, 0);
    }

    void lerpLookAt(Vector3 direction) //Gira hacia el mouse
    {
        direction.y = this.transform.localPosition.y;
        Vector3 relativePos = direction - transform.localPosition;
        Quaternion toRotation = Quaternion.LookRotation(relativePos);
        transform.rotation = toRotation;
    }
}
