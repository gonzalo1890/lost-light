﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_State : MonoBehaviour
{
    public int InventorySphere = 0;
    public State state;
    public Text sphereText;
    // Start is called before the first frame update
    private void Update()
    {
        if (state != null)
            state.Tick(Time.deltaTime);
    }

    public void SphereValue(int value)
    {
        InventorySphere += value;
        sphereText.text = InventorySphere.ToString();
    }
    public int GetSphereValue()
    {
        return InventorySphere;
    }
}
