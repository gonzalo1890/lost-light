﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Updater : MonoBehaviour
{
    private Camera_Follow cameraFollow;
    private Camera_Collision cameraCollision;
    // Start is called before the first frame update
    void Start()
    {
        cameraFollow = GetComponent<Camera_Follow>();
        cameraCollision = transform.GetChild(0).gameObject.GetComponent<Camera_Collision>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateCamera(GameObject target, float MaxDist)
    {
        cameraFollow.CameraFollowObj = target;
        cameraCollision.maxDistance = MaxDist;
    }
}
