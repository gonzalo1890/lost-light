﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    Animator anim;
    Coroutine coroutine;
    private void Start()
    {
        Time.timeScale = 1f;
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            coroutine = StartCoroutine(StartGame());
        }
    }
    IEnumerator StartGame()
    {
        anim.SetTrigger("Start");
        yield return new WaitForSeconds(14);
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
    }
}
