﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Interaction/Raycast")]
public class Interaction_Raycast : Interaction
{
    private Player_State ps;
    public override void Execute(float d)
    {
        if(ps == null)
        {
            ps = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_State>();
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(.5f, .5f, 0f));
            if (Physics.Raycast(ray, out RaycastHit hit, 10))
            {
                Imechanic c = hit.transform.GetComponent<Imechanic>();

                if (c != null)
                {
                    c.OnAction(ps);
                }
            }
        }
    }
}
