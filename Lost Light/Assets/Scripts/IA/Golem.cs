﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Golem : MonoBehaviour
{
    private AICharacterControl ai;
    public Transform[] points;
    public int actualPoint;
    public Animator anim;
    void Start()
    {
        ai = GetComponent<AICharacterControl>();
    }

    private void Update()
    {
        if(Vector3.Distance(ai.target.position, transform.position) < 1)
        {
            anim.SetBool("Walk", false);
        }else
        {
            anim.SetBool("Walk", true);
        }
    }
    void Angry()
    {
        actualPoint++;
        if (actualPoint < points.Length - 1)
        {
            ai.SetTarget(points[actualPoint]);
        }else
        {
            actualPoint = 0;
            ai.SetTarget(points[actualPoint]);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Player_Move>() != null)
        {
            if (Vector3.Distance(transform.position, points[actualPoint].transform.position) < 2)
            {
                Angry();
            }
        }
    }
}
