﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    private int actualLevel = 0;
    public Transform[] positions;
    public GameObject Player;
    public GameObject gameOverObj;
    public GameObject Niebla;
    public AudioSource Sonido;
    public GameObject esfera;
    public GameObject perdidoFinal;
    public Image FadePanel;
    public GameObject credits;
    public GameObject directionaLight;
    public GameObject PostProssesing;
    public GameObject[] lights;
    public AudioClip creditsMusic;

    public GameObject Pause;
    private bool pauseOn = false;
    void Start()
    {
        Time.timeScale = 1f;
        actualLevel = PlayerPrefs.GetInt("actualLevel");
        LoadlLevel(actualLevel);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F8))
        {
            QualitySettings.SetQualityLevel(0, true);
            directionaLight.SetActive(false);
            PostProssesing.SetActive(false);
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].SetActive(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.F9))
        {
            QualitySettings.SetQualityLevel(1, true);
            directionaLight.SetActive(true);
            PostProssesing.SetActive(true);
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].SetActive(true);
            }
        }
        if (Input.GetKeyDown(KeyCode.F10))
        {
            QualitySettings.SetQualityLevel(2, true);
            directionaLight.SetActive(true);
            PostProssesing.SetActive(true);
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].SetActive(true);
            }
        }
        if (Input.GetKeyDown(KeyCode.F11))
        {
            QualitySettings.SetQualityLevel(3, true);
            directionaLight.SetActive(true);
            PostProssesing.SetActive(true);
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].SetActive(true);
            }
        }
        if (Input.GetKeyDown(KeyCode.F12))
        {
            PlayerPrefs.DeleteAll();
            RestartGame();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseOn = !pauseOn;
            Pause.SetActive(pauseOn);
            if (pauseOn)
            {
                Time.timeScale = 0f;
            }
            else
            {
                Time.timeScale = 1f;
            }
        }

        if (pauseOn)
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                SceneManager.LoadScene("Menu", LoadSceneMode.Single);
            }
            if (Input.GetKeyDown(KeyCode.F2))
            {
                SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
            }
            if (Input.GetKeyDown(KeyCode.F3))
            {
                Application.Quit();
            }
        }
    }

    void LoadlLevel(int level)
    {

        if(level == 0)
        {
            StartPosition(positions[0].position);
        }
        if(level == 1)
        {
            positions[1].GetComponent<SphereCollider>().enabled = false;
            StartPosition(positions[1].position);
        }
        if(level == 2)
        {
            positions[2].GetComponent<SphereCollider>().enabled = false;
            StartPosition(positions[2].position);
        }
    }

    void StartPosition(Vector3 pos)
    {
        Player.transform.position = pos;
    }

    public void GameOver()
    {
        gameOverObj.SetActive(true);
        Invoke("ResetScene", 3);
    }
    public void ResetScene()
    {
        SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
    }

    public void FinalBoss()
    {
        Sonido.Play();
        Niebla.SetActive(true);
        Instantiate(esfera, new Vector3(perdidoFinal.transform.position.x, perdidoFinal.transform.position.y, perdidoFinal.transform.position.z + 2), transform.rotation);
    }

    public void Ending()
    {
        FadePanel.gameObject.SetActive(true);
        GetComponent<AudioSource>().clip = creditsMusic;
        GetComponent<AudioSource>().Play();
        //FadePanel.CrossFadeAlpha(1f, 2.0f, true);
        Invoke("Credits", 5);
    }

    public void Credits()
    {
        credits.SetActive(true);
        Invoke("RestartGame", 10);
    }

    public void RestartGame()
    {
        PlayerPrefs.SetInt("actualLevel", 0);
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }
}
