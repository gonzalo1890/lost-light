﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGame : MonoBehaviour
{
    public int SavePoint = 0;
    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Player_State>() != null)
        {
            PlayerPrefs.SetInt("actualLevel", SavePoint);
        }
    }
}
